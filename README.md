# Properties API

## Prerequisites

- [Node.js](https://nodejs.org/en/) 6 or higher (for proper ES2015 support). Can be easily installed with [nvm](https://github.com/creationix/nvm): `nvm install 6.10`
- (Optional) [Yarn](https://yarnpkg.com/) instead of [npm](https://www.npmjs.com/) for blazing fast and reliable dependency management

## Installation

1. `yarn` (or `npm install`)
2. `cp .env.example .env` (you may want to change the defaults in `.env` file)

## Commands

- `yarn start` (or `npm start`) starts server
- `yarn dev` (or `npm run dev`) starts server in development mode (with auto-restarting on changes)
- `yarn test` (or `npm run test`) runs test suite
- `yarn lint` (or `npm run lint`) runs linter
- See `scripts` in `package.json` for more
