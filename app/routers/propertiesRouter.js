const path = require('path');
const express = require('express');
const readJSON = require('../utils/readJSON');
const getCoords = require('../utils/getCoords');

const router = express.Router();

router.get('/properties', (req, res, next) => {
  readJSON(path.join(__dirname, '../data/properties.json'))
    .then(items => Promise.all([
      items,
      ...items.map(item => getCoords(item.address)),
    ]))
    .then(([items, ...coords]) => {
      items.forEach((item, index) => {
        item.coords = coords[index];
      });
      return items;
    })
    .then(items => res.json(items))
    .catch(err => next(err));
});

module.exports = router;
