const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const compression = require('compression');
const notFoundErrorHandler = require('./middleware/notFoundErrorHandler');
const catchAllErrorHandler = require('./middleware/catchAllErrorHandler');
const propertiesRouter = require('./routers/propertiesRouter');

class App {

  constructor() {
    this.express = express();

    this.setupBeforeMiddleware();
    this.setupRouters();
    this.setupAfterMiddleware();
  }

  setupBeforeMiddleware() {
    this.express.use(morgan('dev'));
    this.express.use(cors());
    this.express.use(compression());
  }

  setupAfterMiddleware() {
    this.express.use(notFoundErrorHandler);
    this.express.use(catchAllErrorHandler);
  }

  setupRouters() {
    this.express.use(propertiesRouter);
  }
}

module.exports = App;
