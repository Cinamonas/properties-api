jest.mock('./utils/readJSON', () => jest.fn().mockReturnValue(Promise.resolve([])));
jest.mock('./utils/getCoords', () => jest.fn().mockReturnValue(Promise.resolve(null)));

const request = require('supertest');
const App = require('./App');
const readJSON = require('./utils/readJSON');
const getCoords = require('./utils/getCoords');

const app = new App().express;

describe('API', () => {

  describe('/properties', () => {

    describe('GET /', () => {

      it('should return properties with coords', () => {
        readJSON.mockReturnValueOnce(Promise.resolve([
          { id: 123 },
          { id: 456 },
        ]));
        getCoords.mockReturnValueOnce(Promise.resolve({ lat: 1, lng: 9 }));
        getCoords.mockReturnValueOnce(Promise.resolve({ lat: 2, lng: 8 }));

        return request(app).get('/properties')
          .expect(200)
          .then((res) => {
            expect(res.body).toEqual([
              {
                id: 123,
                coords: { lat: 1, lng: 9 },
              },
              {
                id: 456,
                coords: { lat: 2, lng: 8 },
              },
            ]);
          });
      });

    });

  });

});
