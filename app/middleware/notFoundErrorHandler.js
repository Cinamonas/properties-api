const { NOT_FOUND } = require('http-status-codes');

function notFoundErrorHandler(req, res) {
  res.sendStatus(NOT_FOUND);
}

module.exports = notFoundErrorHandler;
