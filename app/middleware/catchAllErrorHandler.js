const { INTERNAL_SERVER_ERROR } = require('http-status-codes');

function catchAllErrorHandler(err, req, res, next) { // eslint-disable-line no-unused-vars
  console.error(err.stack || err); // eslint-disable-line no-console

  res.sendStatus(INTERNAL_SERVER_ERROR);
}

module.exports = catchAllErrorHandler;
