jest.mock('@google/maps', () => {
  const client = {
    geocode: jest.fn(),
  };
  return {
    client,
    createClient: jest.fn(() => client),
  };
});

const googleMapsApiKey = 'fooBarBaz123';
process.env.GOOGLE_MAPS_API_KEY = googleMapsApiKey;

const maps = require('@google/maps');
const getCoords = require('./getCoords');

describe('getCoords util', () => {

  it('should create Google Maps API client', () => {
    expect(maps.createClient).toBeCalledWith({
      key: googleMapsApiKey,
    });
  });

  it('should get coords from Google Maps API', () => {
    maps.client.geocode = (query, callback) => {
      if (query.address !== 'Foo Bar USA') {
        callback(null, null);
        return;
      }
      callback(null, {
        json: {
          results: [{
            geometry: {
              location: { lat: 4, lng: 2 },
            },
          }],
        },
      });
    };

    return getCoords({ line1: 'Foo', line2: ' Bar ', country: 'USA' })
      .then((coords) => {
        expect(coords).toEqual({ lat: 4, lng: 2 });
      });
  });

  it('should remove address head property until coords are found', () => {
    maps.client.geocode = (query, callback) => {
      if (query.address !== 'Baz USA') {
        callback(null, null);
        return;
      }
      callback(null, {
        json: {
          results: [{
            geometry: {
              location: { lat: 1, lng: 1 },
            },
          }],
        },
      });
    };

    return getCoords({ line1: 'Bar', line2: 'Baz', country: 'USA' })
      .then((coords) => {
        expect(coords).toEqual({ lat: 1, lng: 1 });
      });
  });

  it('should return null if coords could not be found', () => {
    maps.client.geocode = (query, callback) => {
      callback(null, null);
    };

    return getCoords({ line1: 'Baz', line2: 'Qux' })
      .then((coords) => {
        expect(coords).toBe(null);
      });
  });

});
