const _ = require('lodash');
const maps = require('@google/maps');

const client = maps.createClient({
  key: process.env.GOOGLE_MAPS_API_KEY,
});
const store = {};

function getCoords(address) {
  if (!address) {
    return Promise.resolve(null);
  }

  const formattedAddress = formatAddress(address);

  return Promise.resolve()
    .then(() => {
      if (_.has(store, formattedAddress)) {
        return store[formattedAddress];
      }
      return geocode(formattedAddress);
    })
    .then((coords) => {
      store[formattedAddress] = coords;

      if (coords) {
        return coords;
      }

      // attempt to find coords with less noisy address
      return getCoords(getAddressTail(address));
    });
}

function geocode(address) {
  return new Promise((resolve, reject) => {
    client.geocode({ address }, (err, response) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(_.get(response, 'json.results[0].geometry.location'));
    });
  });
}

function formatAddress(address) {
  return _(address)
    .values()
    .map(_.trim)
    .reject(_.isEmpty)
    .join(' ');
}

function getAddressTail(address) {
  const tail = _.pick(address, _(address).keys().tail().value());
  return !_.isEmpty(tail) ? tail : null;
}

module.exports = getCoords;
