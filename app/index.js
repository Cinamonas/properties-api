require('dotenv').config({ silent: true });

const http = require('http');
const App = require('./App');

const app = new App();
const server = http.createServer(app.express);

start(process.env.PORT);

function start(port) {
  server.listen(port);

  server.on('listening', () => {
    console.log(`Server started on port ${port}…`); // eslint-disable-line no-console
  });
}
